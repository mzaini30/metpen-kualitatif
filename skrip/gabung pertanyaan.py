satu = """Nutuk Beham
Menjamu Benua
Adat Behuma
Adat Beyar Niat"""
satu = satu.splitlines()

for x in satu:
	print """Apakah tradisi {isi} dapat membuat Anda memahami bahwa masih ada kekurangan dan mencoba untuk melakukan lebih baik lagi ke depannya?
Nilai-nilai apa saja yang diperoleh dari tradisi {isi}?
Apakah tradisi {isi} dapat membuat Anda mengubah sikap menjadi lebih tepat dalam menghadapi masalah hidup?
Apakah tradisi {isi} meningkatkan komitmen terhadap tujuan hidup yang telah ditetapkan?
Apakah tradisi {isi} dapat meningkatkan potensi pribadi menjadi lebih baik lagi?
Apakah tradisi {isi} mampu menguatkan dukungan sosial?""".format(isi = x)